# Goalfeed JS

Simple package to react to goalfeed.ca events

## How To Use

Simply instantiate the object with a valid username and password and then handle goal events as a callback

```
const Goalfeed = require('goalfeed');
const goalfeed = new Goalfeed('email', 'password')

goalfeed.onGoal((data) => {
  console.log('client action');
});
```
