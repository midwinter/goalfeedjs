var axios = require('axios');
var Pusher = require('pusher-js');

class Goalfeed {
    constructor(username, password, debug=false) {

        Pusher.logToConsole = debug;
        this.goal_callback;
        this.is_connected = false;
        this.connecting = true;

        this.pusher = new Pusher('bfd4ed98c1ff22c04074', {

            enabledTransports: ['ws', 'http'],
            httpHost: 'feed.goalfeed.ca',
            httpPort: '8080',
            wsPort: '8080',
            wsHost: 'feed.goalfeed.ca',
            authorizer: (channel, options) => {
                return {
                    authorize: async (socketId, callback) => {
                        try {
                            const resp = await axios.post('https://goalfeed.ca/feed/auth', {
                                username: username,
                                password: password,
                                connection_info: JSON.stringify({ socket_id: socketId })
                            });
                            this.is_connected = true;
                            this.connecting = false;
                            callback(false, { auth: resp.data.auth });
                        } catch (e) {
                            this.is_connected = false;
                            this.connecting = false;
                            console.log(e)
                        }
                    }
                };
            }
        });

        this.channel = false;

        this.pusher.connection.bind('error', function (err) {
        this.connecting = false;
            console.log(err)
        });

        this.pusher.connection.bind('connecting', () => {
        this.connecting = true;
            if(debug) {
                console.log('connecting')
            }
        });


        this.channel = this.pusher.subscribe('private-goals');
        this.channel.bind('goal', data => {
            if (typeof this.goal_callback === "function") {
                this.goal_callback(data)
            }
        })
    }

    onGoal(callback) {
        this.goal_callback = callback
    }
}
module.exports = Goalfeed
